
#include <iostream>
#include "utils.h"

using namespace std;
int main() {
	int errors = 0;
	errors = errors + (assignSomaticStatus("T", "A", "AC") -2 );
	errors = errors + (assignSomaticStatus("T", "C", "C") -1 );
	errors = errors + (assignSomaticStatus("C", "C", "C") -0 );
	errors = errors + (assignSomaticStatus("T", "CT", "A") -4);
	errors = errors + (assignSomaticStatus("T", "CT", "C") -3 );
	errors = errors + (assignSomaticStatus("T", "CT", "ACT") -2 );
	errors = errors + (assignSomaticStatus("T", "C", "ACT")  - 4);
//	errors = errors + (assignSomaticStatus("T", "", "C")  - 4);
//	errors = errors + (assignSomaticStatus("", "", "")  - 4);
//	errors = errors + (assignSomaticStatus("", "C", "C")  - 4);

	if(errors == 0)
		cout << "Regression test passed" << endl;

	else {
		cout << "Regression test failed "  << errors << "times " << endl;
	}

	return 0;
}
