/*
 * ConditionalTumourDistr.cpp
 *
 *  Created on: Sep 30, 2014
 *      Author: malvinajosephidou
 */
#include <numeric>
#include <cassert>

#include "ConditionalDistr.h"
#include "TumourSample.h"
#include "rungibbs.h"
#include "NormalSample.h"
#include "AllelicState.h"

long double getBetaForDist(int relative_distance, double mutrate) {
	long double beta = 0;
	switch (relative_distance) {
		case 0:
	//		beta  =  1;
			beta  =  0;
			break;
		case 1:
			beta = mutrate;
			break;
		case 2:
			/* Poor mixing if this is too low*/
			beta = mutrate * mutrate;
			break;
		default:
			beta = mutrate * mutrate;
			break;
	}
	return beta;
}

int getDistanceBetweenStates(AllelicState tumour, AllelicState normal) {
	const vector <bool> tumour_bool_state = tumour.getIs_allele_present();
	const vector <bool> normal_bool_state = normal.getIs_allele_present();
	int dist = evolutionary_distance(tumour_bool_state, normal_bool_state);
	return dist;
}
map <AllelicState, long double> getBetaForFixedNormal(AllelicState normal_state, double mutrate) {
	const vector <AllelicState> tumour_states = TumourSample::get_tumour_states();
	map <AllelicState, long double > betas;
	vector <long double> interm(tumour_states.size());

	for (int i = 0; i < tumour_states.size(); i++) {
		//int relative_distance = getDistanceBetweenStates(tumour_states.at(i), normal_state);
		//Chain does not mix if we use 2 or the actual distance
		int relative_distance = getDistanceBetweenStates(tumour_states.at(i), normal_state);
		interm.at(i) = getBetaForDist(relative_distance, mutrate);
		betas[tumour_states.at(i)] = interm.at(i);
	}
	/* Will test this gives same result as current code and will then change it so that beta for normal and tumour the same is 1. */
	betas[normal_state] =  1 - accumulate(interm.begin(), interm.end(), 0.0 );
	return betas;
}
map <AllelicState, map < AllelicState, long double> > ConditionalTumourDistr::getBeta(double mutrate) {
	const vector <AllelicState> normal_states = NormalSample::get_normal_states();
	map < AllelicState, map <AllelicState, long double> > beta_hyperp;
	for(int i = 0 ; i < normal_states.size(); i++) {
		map <AllelicState, long double > beta = getBetaForFixedNormal(normal_states.at(i), mutrate);
		beta_hyperp[normal_states.at(i)] = beta ;
	}
	return beta_hyperp;
}

ConditionalTumourDistr::ConditionalTumourDistr() {
	setAllelicStates(TumourSample::get_tumour_states());
}

void
ConditionalTumourDistr::init(const Data& data, const GibbsSettings &gibbs_settings) {
	set_likelihood_hyperparameter(data.depth, gibbs_settings.MedianTumourCoverage, gibbs_settings.alpha);
	set_likelihood(data);
}

vector <long double> ConditionalTumourDistr::getPriors(const vector <int> &previousSamples,
		const long double &weight,  const int &x,  const map <AllelicState, long double> &thetas,
		const map <AllelicState, map <AllelicState, long double> > &beta_hyperp) {

	AllelicState normal_state = AllelicState(previousSamples.at(0));
	map <AllelicState, long double> betas = beta_hyperp.at(normal_state);

	assert(x != 0);
	const vector <AllelicState> allelic_states = TumourSample::get_tumour_states();
	const AllelicState current_normal_state = AllelicState(previousSamples.at(0));

    vector <long double> priors(allelic_states.size(), 0.0);
    vector <int> occurences(allelic_states.size(), 0);

    for( size_t i = 0 ; i < allelic_states.size() ; i++) {
        priors.at(i) = weight * betas.at(allelic_states.at(i));

        /* Number of samples matching this state. */
        occurences.at(i) = count(previousSamples.begin(),
                                 previousSamples.end(), allelic_states.at(i).getInteger());

        /* Exclude count for the sample you are building conditional for */
        if(allelic_states.at(i).getInteger() == previousSamples.at(x)) {
            occurences.at(i)--;
        }
    }

    for( size_t i = 0 ; i < allelic_states.size() ; i++) {
    	priors.at(i) = (priors.at(i) + occurences.at(i))/
    			(weight + previousSamples.size()-1 );
    }

    assert( fabs (accumulate(priors.begin(), priors.end(), 0.0 ) - 1 ) <= EPSILON);
    return priors;
}

