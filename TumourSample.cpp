/*
 * TumourSample.cpp
 *
 *  Created on: Sep 24, 2014
 *      Author: joseph07
 */

#include "TumourSample.h"

const vector <AllelicState> TumourSample::states = TumourSample::create_tumour_states();

const vector <AllelicState> TumourSample::create_tumour_states() {
		vector <AllelicState> tumour_states;
		for(int i = 1; i < 15; i++) {
		    tumour_states.push_back(AllelicState(i));
		}
		return tumour_states;
	}

const vector <AllelicState> TumourSample::get_tumour_states() {
	return TumourSample::states;
}

TumourSample::TumourSample() {
	// TODO Auto-generated constructor stub
}

TumourSample::~TumourSample() {
	// TODO Auto-generated destructor stub
}
