cd ~/repos/multisnv
cmake CMakeLists.txt
make
actual=testing/actual_BAM_allRegions.vcf

./multisnv --seed 43 -N 4 --bam ~/repos/multisnv/testing/0002_Normal_Blood_Header_sorted.bam.chr21.bam ~/repos/multisnv/testing/0002_Region_01_Header_sorted.bam.chr21.bam ~/repos/multisnv/testing/0002_Region_03_Header_sorted.bam.chr21.bam  ~/repos/multisnv/testing/0002_Region_04_Header_sorted.bam.chr21.bam --minMapQual 30 --minBase 20 -f $actual --print 0 --conv 1 --mva 2 --fasta ~/reference/hg19.fa

expected=testing/expected_BAM_allRegionsNEW.vcf
if cmp -s "$actual" "$expected"
then
echo "Regression test passed"
else
echo "Regression test 1 has failed. Output files $actual and $expected are different!"
fi


actual=testing/actual_BAM_test2.vcf

./multisnv --seed 43 -N 4 --bam ~/repos/multisnv/testing/0002_Normal_Blood_Header_sorted.bam.chr21.bam ~/repos/multisnv/testing/0002_Region_01_Header_sorted.bam.chr21.bam ~/repos/multisnv/testing/0002_Region_03_Header_sorted.bam.chr21.bam  ~/repos/multisnv/testing/0002_Region_04_Header_sorted.bam.chr21.bam --minMapQual 30 --minBase 20 -f $actual --print 0 --conv 1 --mva 2 --fasta ~/reference/hg19.fa --regions chr21:300-9812989

expected=testing/expected_BAM_test2NEW.vcf

if cmp -s "$actual" "$expected"
then
echo "Regression test passed"
else
echo "Regression test 2 has failed. Output files $actual and $expected are different!"
fi

actual=testing/actual_BAM_BedRegions.vcf

./multisnv --seed 43 -N 4 --bam ~/repos/multisnv/testing/0002_Normal_Blood_Header_sorted.bam.chr21.bam ~/repos/multisnv/testing/0002_Region_01_Header_sorted.bam.chr21.bam ~/repos/multisnv/testing/0002_Region_03_Header_sorted.bam.chr21.bam  ~/repos/multisnv/testing/0002_Region_04_Header_sorted.bam.chr21.bam --minMapQual 30 --minBase 20 -f $actual --print 0 --conv 1 --mva 2 --fasta ~/reference/hg19.fa --bed ~/repos/multisnv/S02972011_Regions.txt

expected=testing/expected_BAM_BedRegionsNEW.vcf

if cmp -s "$actual" "$expected"
then
echo "Regression test passed"
else
echo "Regression test 3 has failed. Output files $actual and $expected are different!"
fi

#./multisnv --seed 43 -N 4 --bam ~/repos/multisnv/testing/0002_Normal_Blood_Header_sorted.bam.chr21.bam ~/repos/multisnv/testing/0002_Region_01_Header_sorted.bam.chr21.bam ~/repos/multisnv/testing/0002_Region_03_Header_sorted.bam.chr21.bam  ~/repos/multisnv/testing/0002_Region_04_Header_sorted.bam.chr21.bam --minMapQual 30 --minBase 20 -f testing/actual_BAM_test1.vcf --print 1 --Mism 100 --conv 1 --mva 2 --fasta ~/reference/hg19.fa --BED ~/repos/multisnv/S02972011_Regions.txt --regions chr21:300-


#./multisnv --seed 43 -N 4 --bam ~/repos/multisnv/testing/0002_Normal_Blood_Header_sorted.bam.chr21.bam ~/repos/multisnv/testing/0002_Region_01_Header_sorted.bam.chr21.bam ~/repos/multisnv/testing/0002_Region_03_Header_sorted.bam.chr21.bam  ~/repos/multisnv/testing/0002_Region_04_Header_sorted.bam.chr21.bam --minMapQual 30 --minBase 20 -f testing/actual_BAM_test3.vcf --print 1 --Mism 100 --conv 1 --mva 2 --fasta ~/reference/hg19.fa --regions chr21:300-3000000

#./multisnv --seed 43 -N 4 --bam ~/repos/multisnv/testing/0002_Normal_Blood_Header_sorted.bam.chr21.bam ~/repos/multisnv/testing/0002_Region_01_Header_sorted.bam.chr21.bam ~/repos/multisnv/testing/0002_Region_03_Header_sorted.bam.chr21.bam  ~/repos/multisnv/testing/0002_Region_04_Header_sorted.bam.chr21.bam --minMapQual 30 --minBase 20 -f testing/actual_BAM_test4.vcf --print 1 --Mism 100 --conv 1 --mva 2 --fasta ~/reference/hg19.fa --regions chr21