
cd ~/repos/multisnv
cmake CMakeLists.txt
make
actual=testing/actual_BAM_test2.vcf

./multisnv --seed 43 -N 4 --bam ~/repos/multisnv/testing/0002_Normal_Blood_Header_sorted.bam.chr21.bam ~/repos/multisnv/testing/0002_Region_01_Header_sorted.bam.chr21.bam ~/repos/multisnv/testing/0002_Region_03_Header_sorted.bam.chr21.bam  ~/repos/multisnv/testing/0002_Region_04_Header_sorted.bam.chr21.bam --minMapQual 30 --minBase 20 -f $actual --print 0 --conv 1 --mva 2 --fasta ~/reference/hg19.fa --regions chr21:300-9812989
expected=testing/expected_BAM_test2NEW.vcf

if cmp -s "$actual" "$expected"
then
echo "Regression test passed"
else
echo "Regression testing has failed. Output files are different!"
fi
