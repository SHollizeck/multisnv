/*
 * Read.h
 *
 *  Created on: Feb 19, 2015
 *      Author: joseph07
 */

#ifndef READ_H_
#define READ_H_

class Read {
public:
	Read();
	virtual ~Read();
	Read(int baseIdx, long double err, int start_dist, int end_dist, bool is_rev);

	int base;
	long double error_prob;
	int dist_start;
	int dist_end;
	bool isReverse;
};

#endif /* READ_H_ */
