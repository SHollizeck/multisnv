/*
 * write_snp_header.cpp
 *
 *  Created on: Sep 20, 2014
 *      Author: malvinajosephidou
 */

#include <iostream>
#include<fstream>
#include<istream>
#include<iomanip>
#include<string>
#include<sstream>
#include <cmath>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <iterator>
#include <map>
#include <time.h>
#include <utility>
#include <cstring>
#include <functional>
#include <numeric>
#include <boost/regex.hpp>
#include <boost/program_options.hpp>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>
#include "kml2mapsVersion.h"
#include "write_snp_header.h"
using namespace std;

void
write_VCF_header(string filename, FilterSettings filter_settings,
		ProgramSettings program_settings, GibbsSettings gibbs_settings, int argc, char* argv[])
{
	ofstream snps;
	snps.open(filename.c_str(), ios_base::out | ios_base::app);
    snps << setprecision(10); snps << fixed; snps << left;

    snps << "##fileformat=VCFv4.0" 								  << endl;
    snps << "##source=multiSNV" << GIT_REVISION 					  << endl;
    snps << "##reference=file:// " <<  program_settings.ref_fasta			  << endl;
    snps << "##phasing=none" 										  << endl;
    snps << "##multiSNV command-line=" ;
	for(int i = 0 ; i < argc ; i++) {
    	snps << argv[i] << " " ;
	}
	snps 															  << endl;
	snps << "##Input bam files=";
	for ( int i =0 ; i < program_settings.bam_files.size(); i++) {
		snps << program_settings.bam_files.at(i) << " " ;
	}
	snps << endl;
	snps << "##Seed used="										  << program_settings.seed << endl;
    snps << "##Number of samples="                                  << program_settings.NumberOfSamples << endl;
    snps << "##Minimun number of variant alleles"
    		" required to consider site="  						  << filter_settings.minVarAlleles << endl;
    snps << "##Minimum normal depth required to consider site="     << filter_settings.min_depth << endl;
    snps << "##Minimum average depth required to consider site="    << filter_settings.min_depth << endl;
    snps << "##Median tumour coverage="                             << gibbs_settings.MedianTumourCoverage << endl;
    snps << "##Median normal coverage="                             << gibbs_settings.MedianNormalCoverage << endl;
    snps << "##Minimum Base Quality=" 							  << filter_settings.ReadQualThreshold << endl;
    snps << "##Minimum Mapping Quality=" 							  << filter_settings.MappingQualThreshold << endl;
    snps << "##Mutation Rate=" 									  << gibbs_settings.mutrate << endl;
    snps << "##Number of Gibbs iterations="						  << gibbs_settings.gibbs_runs << endl;
    snps << "##Thinning parameter=" 								  << gibbs_settings.thin << endl;
    snps << "##Burn in period fraction="  						  << gibbs_settings.burnin << endl;
    snps << "##Run convergence check=" 							  << gibbs_settings.checkConv << endl;
    snps << "##Maximum number of Gibbs iterations="  				  << gibbs_settings.maxIter << endl;
    snps << "##Minimum number of reads to count site in normal as triallelic=" 	  << filter_settings.minVariantReadsForTriallelicSite << endl;
    snps << "##Hyperparameters of the Dirichlet prior on variant allele frequency in tumour=" << gibbs_settings.alpha  << endl;
    snps << "##Hyperparameters of the Dirichlet prior on variant allele frequency in normal=" << gibbs_settings.alphaN << endl;
    snps << "##Scale of hyperparameters of the Dirichlet prior on the sampling probabilities of each tumour state=" 		  << gibbs_settings.weight << endl;
    snps << "##Flag as NORMAL_CONTAMINATION if the variant allele frequency in the normal is greater than="       << filter_settings.normalContaminationThreshold << endl;
    snps << "##Flag as WEAKLY_SUPPORTED_VARIANT if the maximum variant allele frequency "
    		"observed across all samples with the mutation is less than="  << filter_settings.weakEvidenceThreshold << endl;
    snps << "##INFO=<ID=NS,Number=1,Type=Integer,Description=\"Number of Samples With Data\">" 		    << endl;
    snps << "##INFO=<ID=DISTR,Number=1,Type=String,Description=\"Allelic composition in all samples\">" << endl;
    snps << "##INFO=<ID=SB,Number=1,Type=Float,Description=\"Strand bias of variant\">" 				<< endl;
    snps << "##FORMAT=<ID=A,Number=1,Type=String,Description=\"Allelic Composition\">"					<< endl;
    snps << "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">"							<< endl;
    snps << "##FORMAT=<ID=GQ,Number=1,Type=Float,Description=\"Genotype Quality\">" 					<< endl;
    snps << "##FORMAT=<ID=SS,Number=1,Type=Integer,Description=\"Somatic Status,Integer Variant status relative to Normal,0=wildtype,1=germline,2=somatic,3=LOH,4=unknown\">" 					<< endl;
    snps << "##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Depth\">" 								<< endl;
    snps << "##FORMAT=<ID=BCOUNT,Number=4,Type=Integer,Description=\"Allele counts\">"					<< endl;
	snps << "##FILTER=<ID=LOW_QUAL,Description=\"Phred-scaled "
			"overall SNV quality is less than " << filter_settings.flag_low_SNV_qual << "\">" 		    << endl;
	snps << "##FILTER=<ID=MAX_ITER,Description=\"Gibbs sampler failed to converge\">" 					<< endl;
	snps << "##FILTER=<ID=STRAND_BIAS,Description=\"Significant strand-bias\">" 						<< endl;
	snps << "##FILTER=<ID=EXCESSIVE_MUTS,Description=\"Event does not seem biologically plausible\">" 	<< endl;
	snps << "##FILTER=<ID=LOW_QUAL_NORMAL,Description=\"Normal sample has excessive mismatches"
			" spanning more than 2 alleles\">" 	<< endl;
	snps << "##FILTER=<ID=BAD_READS,Description=\"More"
			" than " <<  filter_settings.fraction_failing_QC << " of reads at position fail "
			"minimum mapping/base quality criteria\">"												    << endl;
	snps << "##FILTER=<ID=LOW_DEPTH_IN_NORMAL,Description=\"Normal depth is lower "
			"than " << filter_settings.flag_low_depth << "\">" 	 										<< endl;
	snps << "##FILTER=<ID=LOW_AVERAGE_DEPTH,Description=\"Average depth is lower "
			"than " << filter_settings.flag_low_depth << "\">" 											<< endl;
	snps << "##FILTER=<ID=CLUSTERED_READS,Description=\"Median distance from start or end is "
			"less than 11 and median absolute deviation from start or end is less than 4\">" 		    << endl;
	snps << "##FILTER=<ID=WEAKLY_SUPPORTED_VARIANT,Description=\"Maximum variant allele frequency "
			"observed across all samples with mutation is less than " << filter_settings.weakEvidenceThreshold << "\">" << endl;
	snps << "##FILTER=<ID=NORMAL_CONTAMINATION,Description=\"Variant is observed in the normal sample "
			"at a frequency greater than " << filter_settings.normalContaminationThreshold
			<< " AND there is more than 1 variant read in the normal\">" 	<< endl;
	snps << "##FILTER=<ID=HOMOPOLYMER,Description=\"Variant detected close to a flanking homopolymer of at "
			"least " << filter_settings.homopolLengthThreshold << " repetitive bases\">" 								<< endl;
	snps << "##FILTER=<ID=PASS,Description=\"Accept\">" 												<< endl;
    snps << "#CHROM"  << '\t' << "POS"   << '\t' << "ID" << '\t' << "REF" << '\t'
    	 << "ALT"     << '\t' << "QUAL" << '\t' << "FILTER"   << '\t'  << "INFO" << '\t'
    	 << "FORMAT" << '\t' << "N"     << '\t';

    for(int i = 1; i < program_settings.NumberOfSamples; i++) {
        snps << "T" <<i;
    if(i < program_settings.NumberOfSamples - 1)
        snps << '\t';
	}
	snps << endl;
    snps.close();

    return;
    }


void
write_features_file_header(string filename, int NumberOfSamples) {
	ofstream outp;
	outp.open(filename.c_str(), ios_base::out | ios_base::app);
	outp    << "#CHROM"				<< "," << "POS"							<< ","
			<< "NORMAL_CONT_COUNTS" << "," << "NORMAL_DEPTH"    		    << ","
			<< "MEAN_DEPTH"			<< "," << "FRACTION_FAILING_QC" 		<< ","
			<< "ARE_READS_CLUSTERED"<< ","
			<< "HOMOPOLYMER_RUN"	<< "," << "STRAND_BIAS"			        << ","
			<< "VARIANT_QUALITY"    << "," << "FAILED_TO_CONVERGE"				  << ","
			<< "START_MEDIAN_DIST"  << "," <<  "START_MEDIAN_ABSOLUTE_DEVIATION"  << ","
			<< "END_MEDIAN_DIST"	<< "," <<  "END_MEDIAN_ABSOLUTE_DEVIATION"    << "," << "N_VARIANT_FREQ";

			for(int i=1; i< NumberOfSamples ;i++) {
				outp << "," << "T" << i << "_VARIANT_FREQ";
			}
	outp << endl;
	outp.close();
	return;

}
