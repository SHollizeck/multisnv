
//
//  rungibbs.h
//  
//
//  Created by Malvina Josephidou on 06/02/2013.
//
//

#ifndef ____rungibbs__
#define ____rungibbs__

#include <iostream>
#include <vector>
#include <map>
#include <boost/interprocess/smart_ptr/unique_ptr.hpp>

#include "Gibbs.h"
#include "Settings.h"
using namespace std;
#define EPSILON 0.000000001

string add_delimiter_to_string(string input, string delimiter);
string reformat_genotype(string genotype);
/* SOS : Never pass this by reference!! It will change the ptr_results->MAP element!!!!) */
vector <string> find_unique_vector_elements(vector <string> vec);
vector <int> getValuesFromRandomInitialization(const vector <Gibbs*> &ptr_gibbs);

struct sort_pred {
    bool operator()(const pair<string,int> &left, const pair<string,int> &right) {
        return right.second < left.second;
	}
};
        
struct strlength{
	bool operator() ( const string& a, const string& b ){
                return a.size() < b.size();
	}
};
        
// Functions realted to model: generate sampling probabilities, sample
int evolutionary_distance(const vector <bool> &tumour_state, const vector <bool> &normal_state);
vector <long double> safe_exponentiation( const vector<long double>& probs);
bool check_if_priors_changed( const vector <int> &current, const vector <int> &previous);
double fisher_test(const unsigned &a, const unsigned &b, const unsigned &c, const unsigned &d);
      
#endif /* defined(____rungibbs__) */
