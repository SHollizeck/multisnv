/*
 * multisnv_input_parser.h
 *
 *  Created on: Sep 20, 2014
 *      Author: malvinajosephidou
 */

#ifndef MULTISNV_INPUT_PARSER_H_
#define MULTISNV_INPUT_PARSER_H_

#include <boost/regex.hpp>
#include <boost/program_options.hpp>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>

#include "Settings.h"
#include "GibbsSettings.h"

namespace po = boost::program_options;
using namespace std;

po::options_description
get_multisnv_options_desc(FilterSettings& filter_settings,
		ProgramSettings& program_settings, GibbsSettings& gibbs_settings);

void
finalise_settings(const po::variables_map vm, FilterSettings& filter_settings,
		ProgramSettings& program_settings, GibbsSettings& gibbs_settings);

#endif /* MULTISNV_INPUT_PARSER_H_ */
