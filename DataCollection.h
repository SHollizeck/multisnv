//
////
////  DataCollection.h
////
////
////  Created by Malvina Josephidou on 06/02/2013.
////
////
//
//#ifndef ____BiopsyCollection__
//#define ____BiopsyCollection__
//
//#include <iostream>
//#include <string>
//#include <vector>
//#include<fstream>
//#include<istream>
//#include<sstream>
//#include <cmath>
//#include <stdio.h> //to use fopen etc
//#include <vector>
//#include <algorithm> // for copy
//#include <iterator> // for ostream_iterator
//#include <stdlib.h>
//#include <map>
//#include <utility>
//#include <cstring>
//#include <iterator>
//#include <map>
//#include <RInside.h>
//#include <Rcpp.h>
//
//#include "Settings.h"
//#include "GibbsSettings.h"
//#include "SummaryResults.h"
//#include "Locus.h"
//#include "BiopsySample.h"
//
//using namespace std;
//
//class DataCollection {
//
//protected:
//    int NumberOfBiopsies;
//
//public:
//    static const
//    	string bases;
//    static const
//        map <char, long double> QualityMap;
//    static const
//    	map <char, long double> createMap();
//
//    Locus locus;
//    vector <BiopsySample*> ptr_samples;
//
//    DataCollection(int N);
//	~DataCollection();
//
//	int  getNumberOfBiopsies() const;
//	void setNumberOfBiopsies(int NumberOfBiopsies);
//
//	int  ParsePileupLine(char line_read[]);
//    bool IsCandidateSomatic(const FilterSettings* ptr_filter_settings);
//	void convertqualities(double MaxBaseErrorProb); //TODO: move this to BiopsySample class
//    //void rungibbs(const GibbsSettings &gibbs_settings, RInside &R,
//		//		  const ProgramSettings &program_settings);
//};
//
//#endif /* defined(____BiopsyCollection__) */
