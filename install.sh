mkdir redist
cd redist
git clone https://github.com/pezmaster31/bamtools.git
cd bamtools
mkdir build
cd build
cmake ..
make

cd ../../../
echo "Installing multiSNV..."
cmake -DCMAKE_BUILD_TYPE=Release CMakeLists.txt
make

