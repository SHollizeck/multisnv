/*
 * SampleResults.h
 *
 *  Created on: Feb 24, 2015
 *      Author: joseph07
 */

#ifndef SAMPLERESULTS_H_
#define SAMPLERESULTS_H_


#include "utils.h"
#include "rungibbs.h"

class SampleResultsBasic {
	protected:
		MAPstate state;
	public:
		SampleResultsBasic(Gibbs* gibbs) {
			state = gibbs->getMAPstate();
		}
		SampleResultsBasic() : state(MAPstate()) {}

		MAPstate get_MAP_state() const {
			return state;
		}
};

class SampleResults : public SampleResultsBasic {
	private:
		typedef SampleResultsBasic super;
		variant_type_t somatic_status;

	public:
		SampleResults() : super(), somatic_status(WILDTYPE) {}
		SampleResults(Gibbs* gibbs,
					  MAPstate matched_normal_state,
					  string reference)
					  : super(gibbs)
		{
			somatic_status = assignSomaticStatus(reference, matched_normal_state.get_as_str(),
												  state.get_as_str());
		}
		variant_type_t get_somatic_status() const {
			return somatic_status;
		}

		string get_genotype(char ref, string alternate_alleles) const {
				string allelesAtLocus = ref + alternate_alleles;
				string genotype = "";

				for (int j = 0; j < allelesAtLocus.size() ; j++) {
					string sample_state = this->get_MAP_state().get_as_str();
					if(sample_state.find(allelesAtLocus[j]) != string::npos) {
						stringstream allele;
						allele << j;
						genotype = genotype + allele.str();
					}
				}
				genotype = reformat_genotype(genotype);
				return genotype;
		}

		bool operator ==(const SampleResults &other_sample) const {
			return this->state.get_as_int() == other_sample.state.get_as_int();
		}

		bool operator !=(const SampleResults &other_sample) const {
			return !(*this == other_sample);
		}
};


#endif /* SAMPLERESULTS_H_ */
