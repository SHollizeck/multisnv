//
//  GibbsAnalysis.h
//
//
//  Created by Malvina Josephidou on 06/02/2013.
//
//

#ifndef ____Gibbs__
#define ____Gibbs__
#include <iostream>
#include <string>
#include <vector>
#include<fstream>
#include<istream>
#include<sstream>
#include <cmath>
#include <stdio.h> //to use fopen etc
#include <vector>
#include <algorithm> // for copy
#include <iterator> // for ostream_iterator
#include <stdlib.h>
#include <map>
#include <utility>
#include <cstring>
#include <iterator>
#include <map>

#include "AllelicState.h"
#include "MAPstate.h"
using namespace std;

class Gibbs {
public:
    bool getIs_normal() const;
    void setIs_normal(bool is_normal);
    MAPstate getMAPstate();
    vector<int> samples;
    vector<int> countsBin1;
    vector<int> countsBin2;

    Gibbs() 
		: countsBin1(15, 0) 
		, countsBin2(15, 0) 
		{}
			
private:
	bool is_normal;

};

#endif  /* defined(____Gibbs__) */
