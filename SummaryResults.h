
/*
 *  SummaryResults.h
 *  multisnv_proj
 *
 *  Created by Malvina Josephidou on 25/08/2014.
 *  Copyright 2014 University of Cambridge. All rights reserved.
 *
 */
#ifndef ____SummaryResults__
#define ____SummaryResults__
#include <iostream>
#include <string>
#include <vector>
#include<fstream>
#include<istream>
#include<sstream>
#include <cmath>
#include <stdio.h> //to use fopen etc
#include <vector>
#include <algorithm> // for copy
#include <iterator> // for ostream_iterator
#include <stdlib.h>
#include <map>
#include <utility>
#include <cstring>
#include <iterator>
#include <map>

#include "Settings.h"
using namespace std;

class SummaryResults {
public:
	~SummaryResults();

	static SummaryResults* getInstance();
	static void destroyInstance();
	static const map <variant_type_t, string > create_event_map();
	static const map <variant_type_t, string> event_types;

	void print_summary(double runtime);
	void updateSummary(variant_type_t event_type, bool failed_to_converge);

	int n_strand_bias;
	int n_failed_converge;
	map <variant_type_t, int> n_event_type;
	unsigned int n_runs;
	unsigned long n_sites;

private:
	SummaryResults();

	static bool instance_flag;
	static SummaryResults* singleton;
};



#endif /* defined(____SummaryResults__) */
