////
////  Class.cpp
////
////
////  Created by Malvina Josephidou on 06/02/2013.
////
////
//
//#include <iostream>
//#include <string>
//#include <functional>
//#include <numeric>
//#include <tr1/unordered_map>
//#include "DataCollection.h"
//#include "Settings.h"
//#include "utils.h"
//
//using namespace std;
//
//const map <char, long double> DataCollection::QualityMap = DataCollection::createMap();
//const string DataCollection::bases = "ACGT";
//
//const map <char, long double> DataCollection::createMap() {
//   	map < char, long double > qual_map;
//   	for (int i = 33; i < 126; i++) {
//   		char temp = i;
//   	    qual_map.insert(pair<char,
//   	             long double>(temp, pow(10, -0.1 * (i-33))));
//   	        }
//   return qual_map;
//   }
//
//   DataCollection::DataCollection(int N) {
//        this->NumberOfBiopsies = N;
//		for(int d = 0; d < getNumberOfBiopsies(); d++) {
//			ptr_samples.push_back(new BiopsySample());
//		}
//   }
//
//	DataCollection::~DataCollection() {
//		while(!ptr_samples.empty()) {
//           delete ptr_samples.back(), ptr_samples.pop_back();
//       }
//	}
//
//int DataCollection::ParsePileupLine(char line_read[]) {
//	char* pch;
//	int pos=0;
//
//	locus.chrom = strtok (line_read," \t \n ");
//	pos++;
//
//	locus.sequenceID = strtok (NULL," \t \n ");
//	pos++;
//
//	pch = strtok (NULL," \t \n ");
//	locus.ref = toupper(pch[0]);
//	pos++;
//
//	for(int i = 0; i < getNumberOfBiopsies(); i++) {
//		pch = strtok (NULL," \t \n ");
//
//		if(pch != NULL) {
//			ptr_samples.at(i)->depth = atoi(pch);
//			pos++;
//			pch = strtok (NULL," \t \n ");
//
//			if(pch != NULL) {
//				ptr_samples.at(i)->seq = pch;
//				pos++;
//				pch = strtok (NULL," \t \n ");
//
//				if(pch != NULL) {
//					ptr_samples.at(i)->qual = pch;
//					pos++;
//				}
//			}
//		}
//	}
//	return pos;
//}
//
//int DataCollection::getNumberOfBiopsies() const {
//    return NumberOfBiopsies;
//}
//
//
//void DataCollection::setNumberOfBiopsies(int NumberOfBiopsies) {
//    this->NumberOfBiopsies = NumberOfBiopsies;
//}
//
//bool DataCollection::IsCandidateSomatic(const FilterSettings* ptr_filter_settings) {
//
//	/* Quality control for normal: check minimum and maximum depth requirements */
//    if(ptr_samples.at(0)->depth < ptr_filter_settings-> min_depth
//	   or ptr_samples.at(0)->depth >= ptr_filter_settings-> max_depth)
//		return false;
//
//    /* Tumour samples must all have at least one read for site to be processed */
//	for(int i = 1;i < getNumberOfBiopsies();i++) {
//		if(ptr_samples.at(i)->depth <= 0) {
//			return false;
//		}
//	}
//
//	vector<unsigned int> allelic_counts(4,0);
//    bool runMCMC = false;
//
//	for(int i = 0 ; i < getNumberOfBiopsies(); i++) {
//
//		/* Convert pileup format to bases, update read base counts.*/
//		ptr_samples.at(i)->convertsequences(locus.ref);
//		ptr_samples.at(i)->depth = ptr_samples.at(i)->reads.SeqQual.size();
//		ptr_samples.at(i)->sumBaseCounts();
//
//		/* Sort and take the next to last element (corresponds to minor allele freq)
//		 * If any mismatches found in a sample of depth greater than min, run MCMC */
//		allelic_counts = ptr_samples.at(i)->reads.base_counts;
//		sort(allelic_counts.begin(), allelic_counts.end());
//
//		if(allelic_counts.at(2) >= ptr_filter_settings-> minVarAlleles) {
//			runMCMC = true;
//		}
//	}
//
//	if(runMCMC ==true) {
//		/* Preliminary requirements have been met: proceed to decode
//		 * ASCII qualities into error probabilities */
//		this->convertqualities(ptr_filter_settings->MaxBaseErrorProb);
//		int total_depth = 0;
//        double mean_depth;
//
//		for(int i = 0; i < getNumberOfBiopsies(); i++) {
//			ptr_samples.at(i)->sumBaseCounts();
//
//			/* Need to recompute depth as convertqualities will remove low quality reads */
//			ptr_samples.at(i)->depth = ptr_samples.at(i)->reads.proSeqQual.size();
//            total_depth += ptr_samples.at(i)->depth;
//
//				/* Re-check to see all samples have non-zero depth. */
//				if(ptr_samples.at(i)->depth <= 0) {
//					return false;
//				}
//		}
//		  mean_depth = double(total_depth) / getNumberOfBiopsies();
//
//		/* To run only on samples where the normal has no more than 1 non-ref read. */
//		allelic_counts = ptr_samples.at(0)->reads.base_counts;
//		sort(allelic_counts.begin(),allelic_counts.end());
//
//		if(allelic_counts.at(2) > ptr_filter_settings-> MaxVarNorm
//				or ptr_samples.at(0)->depth < ptr_filter_settings->min_depth
//				or mean_depth < ptr_filter_settings-> min_depth) {
//			return false;
//		}
//
//		runMCMC = false;
//
//        for(int i = 1; i < getNumberOfBiopsies(); i++) {
//			allelic_counts = ptr_samples.at(i)->reads.base_counts;
//			sort(allelic_counts.begin(), allelic_counts.end());
//
//            if(allelic_counts.at(2) >= ptr_filter_settings-> minVarAlleles) {
//				return true;
//			}
//        }
//	}
//    return runMCMC;
//}
//
//void DataCollection::convertqualities(double MaxBaseErrorProb) {
//	pair <char, char> raw_read;
//	pair <int, long double> pro_read;
//
//	for(int i = 0; i < getNumberOfBiopsies(); i++) {
//		for(int j = 0 ; j < ptr_samples.at(i)->depth; j++) {
//			raw_read.first = ptr_samples.at(i)->reads.SeqQual.at(j).first;
//			raw_read.second = ptr_samples.at(i)->qual[j];
//
//			if(raw_read.first != 'X') {
//				pro_read.first = base2ind(raw_read.first);
//
//			if(QualityMap.find(raw_read.second) == QualityMap.end()) {
//				cout << " Unknown ASCII base quality: " << raw_read.second << endl;
//				exit(EXIT_FAILURE);
//			}
//
//			else {
//				pro_read.second = QualityMap.at(raw_read.second);
//			}
//
//			if(pro_read.second < MaxBaseErrorProb) {
//				ptr_samples.at(i)->reads.proSeqQual.push_back(pro_read);
//			}
//
//			else if(pro_read.second >= MaxBaseErrorProb) {
//					if(islower(raw_read.first)) {
//						ptr_samples.at(i)->reads.reverse_base_counts.at(pro_read.first)--;
//					}
//
//					else {
//						ptr_samples.at(i)->reads.forward_base_counts.at(pro_read.first)--;
//					}
//
//			}
//		}
//		}
//	}
//	return;
//}

