/*
 * AllelicState.h
 *
 *  Created on: Sep 20, 2014
 *      Author: malvinajosephidou
 */

#ifndef ALLELICSTATE_H_
#define ALLELICSTATE_H_

#include <vector>
#include <string>
#include <map>
#include "Data.h"
using namespace std;

class AllelicState {

private:
	int integer; /* integer representation of state */
	vector <bool> is_allele_present; /*  indicates which of 4 alleles are present in this state */
	string alleles;
public:
	AllelicState(int x);
	AllelicState();
	bool operator==(const AllelicState& rhs) const;
	bool operator<(const AllelicState& rhs) const;
    string getAlleles() const;
    int getInteger() const;

    const vector<bool> &getIs_allele_present() const;

    void setAlleles(string alleles);
    void setInteger(int integer);
    void setIs_allele_present(const vector<bool> &is_allele_present);

	string form_alleles_string(const vector <bool> &is_allele_present);
	vector <bool> integer_to_binary(int x);
	int total_reads_matching_alleles_in_state(const vector <unsigned int> &total_reads);
};
#endif /* ALLELICSTATE_H_ */
