/*
 * TumourSample.h
 *
 *  Created on: Sep 24, 2014
 *      Author: joseph07
 */

#ifndef TUMOURSAMPLE_H_
#define TUMOURSAMPLE_H_

#include "AllelicState.h"
class TumourSample {
public:
	TumourSample();
	virtual ~TumourSample();
	static const vector <AllelicState> create_tumour_states();
	static const vector <AllelicState> get_tumour_states();

private:
    static const vector <AllelicState> states;
};

#endif /* TUMOURSAMPLE_H_ */
