/*
 * ConditionalDistr.h
 *
 *  Created on: Sep 20, 2014
 *      Author: malvinajosephidou
 */

#ifndef CONDITIONALDISTR_H_
#define CONDITIONALDISTR_H_

#include <stdio.h>
#include <math.h>
#include <cstdlib>
#include <algorithm>  
#include "AllelicState.h"
#include "GibbsSettings.h"
#include "Data.h"

map <AllelicState, long double> getBetaForFixedNormal(AllelicState normal_state, double mutrate);
long double getBetaForDist(int relative_distance, double mutrate);
int getDistanceBetweenStates(AllelicState tumour, AllelicState normal);
vector <long double> getTumourPriors(const vector <int> &previousSamples,
		const long double &weight,  const int &x, const map <AllelicState, long double> &betas);
vector <long double> getNormalPriors( const vector <int> &previousSamples,
                                     const long double &weight, const int &x, const map <AllelicState, long double> &theta);
long double getDelta(int distance);
long double computeDirMulti(const vector <int> &previousSamples, const map <AllelicState,
		long double> &betas, const AllelicState &normal, const long double &weight);

class ConditionalDistr {

private:
    long double likelihood_hyperparameter;
    vector<long double> likelihood;
    vector<long double> posterior;
    vector <int> fixed_states;

protected:
    vector <AllelicState> allelic_states;
    static char ref;

public:
	ConditionalDistr() {}
	virtual ~ConditionalDistr() {}

	static ConditionalDistr* createConditionalDistr(bool is_normal);
    static void setRef(char input);
    static char getRef();

	virtual void init(const Data& data, const GibbsSettings &gibbs_settings)  = 0;
	virtual vector <long double> getPriors(const vector <int> &previousSamples, const long double &weight, const int &x,
			const map <AllelicState, long double> &thetas,
			const map< AllelicState, map <AllelicState, long double> > &beta_hyperp) = 0;

    int sample_from_conditional();

	void set_likelihood_hyperparameter(const int sample_depth,
			int MedianCoverage, long double alpha);
	void set_likelihood(const Data& data);
    void setPosterior(vector<long double> posterior);
	void updateFixedStates(vector<int> fixed_parameters);
	void setAllelicStates(const vector<AllelicState> &allelic_states);
	void updatePosterior(int x, const map <AllelicState, long double> &thetas, const vector <int> &latest_drawn_states,
			const map < AllelicState, map <AllelicState, long double> > &beta_hyperp, long double weight);
	long double get_likelihood_hyperparameter();
	const vector <long double> &getLikelihood();
    const vector<long double> &getPosterior() const;
    const vector<AllelicState> &getAllelicStates() const;
    const vector<int> &getFixedStates() const;
};

class ConditionalNormalDistr : public ConditionalDistr {

public:
	ConditionalNormalDistr();
	virtual ~ConditionalNormalDistr(){}
	static map < AllelicState, long double  > getTheta(AllelicState ref_state);
	virtual void init(const Data& data, const GibbsSettings &gibbs_settings);
	virtual vector <long double> getPriors(
		    		const vector <int> &previousSamples, const long double &weight, const int &x,
		    					const map <AllelicState, long double> &thetas,
		    					const map< AllelicState, map <AllelicState, long double> > &beta_hyperp);
};

class ConditionalTumourDistr : public ConditionalDistr {

public:
	ConditionalTumourDistr();
	virtual ~ConditionalTumourDistr(){}
	static map <AllelicState, map < AllelicState, long double> > getBeta(double mutrate);
	virtual void init(const Data& data, const GibbsSettings &gibbs_settings);
	virtual vector <long double> getPriors(
		    		const vector <int> &previousSamples, const long double &weight, const int &x,
					const map <AllelicState, long double> &thetas,
					const map< AllelicState, map <AllelicState, long double> > &beta_hyperp);
};



#endif /* CONDITIONALDISTR_H_ */
