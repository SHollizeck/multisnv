/*
 * ConditionalNormalDistr.cpp
 *
 *  Created on: Sep 30, 2014
 *      Author: malvinajosephidou
 */
#include <numeric>
#include <cassert>

#include "ConditionalDistr.h"
#include "NormalSample.h"
#include "rungibbs.h"
#include "utils.h"
#include "TumourSample.h"
#include <boost/math/special_functions/gamma.hpp>
ConditionalNormalDistr::ConditionalNormalDistr()
{
	setAllelicStates(NormalSample::get_normal_states());
}

void ConditionalNormalDistr::init (const Data& data,
		const GibbsSettings &gibbs_settings)
{
	set_likelihood_hyperparameter(data.depth,
			gibbs_settings.MedianNormalCoverage, gibbs_settings.alphaN);
	set_likelihood(data);
}

map < AllelicState, long double  > ConditionalNormalDistr::getTheta(AllelicState ref_state)
{
		const vector <AllelicState> &allelic_states = NormalSample::create_normal_states();
		map <AllelicState, long double> prior;
		for(size_t j = 0 ; j < allelic_states.size() ; j++) {
			int distance = evolutionary_distance(allelic_states.at(j).getIs_allele_present(),
					ref_state.getIs_allele_present());
			prior[allelic_states.at(j)] = getDelta(distance);
		}
		return prior;
}
long double getDelta(int distance) {
	const double homRef = 0.9985;
	const double homVar = 0.0001665;
	const double hetRef = 0.000334;
	const double hetVar = 0.0000000833;

	switch(distance) {
		case 0:
			return homRef;
			break;
		case 1:
			return hetRef;
			break;
		case 2:
			return homVar;
			break;
		case 3:
			return hetVar;
			break;
		default:
			cout << "Error in setting prior hyperparameters for normal " << endl;
			exit(EXIT_FAILURE);
	}
}

vector <long double> ConditionalNormalDistr::getPriors( const vector <int> &previousSamples,
		const long double &weight, const int &x,
		const map <AllelicState, long double> &thetas,
		const map <AllelicState, map <AllelicState, long double> > &beta_hyperp)
{
	vector <AllelicState> tumour_states = TumourSample::get_tumour_states();
	for (int k = 0; k < tumour_states.size(); k++) {
		int nk =  count(previousSamples.begin() + 1,
                previousSamples.end(), tumour_states.at(k).getInteger());
	}

	assert(x == 0);
	vector <AllelicState> normal_states = NormalSample::get_normal_states();
	vector<long double> priors(normal_states.size(), 0.0);

	for( size_t i = 0 ; i < normal_states.size() ; i++) {
		map <AllelicState, long double > betas = beta_hyperp.at(normal_states.at(i));
		priors.at(i) = computeDirMulti(previousSamples, betas, normal_states.at(i), weight)*(thetas.at(normal_states.at(i)));
	}

	long double normalizing_factor = accumulate(priors.begin(), priors.end(), 0.0);
	for( size_t i = 0 ; i < normal_states.size() ; i++) {
		priors.at(i) = priors.at(i) / normalizing_factor;
	}
	return priors;
}

long double computeDirMulti(const vector <int> &previousSamples, const map <AllelicState, long double> &betas,
		const AllelicState &normal, const long double &weight) {
	vector <AllelicState> tumour_states = TumourSample::get_tumour_states();
	long double result = 1;
	long double temp = 0;
	long double betak;
	int nk;
	for (int k = 0; k < tumour_states.size(); k++) {
		betak = betas.at(tumour_states.at(k));
		/* Skip normal sample when counting */
		nk =  count(previousSamples.begin() + 1,
                previousSamples.end(), tumour_states.at(k).getInteger());
		assert(nk >= 0);

		if( nk == 0) {
			temp = 1;
		}

		else {
			betak = weight * betak;
			temp =  tgamma(nk + betak ) / tgamma(betak);
		}
		result = result * temp;
	}
	return result;
}
