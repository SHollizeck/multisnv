/*
 * NormalSample.h
 *
 *  Created on: Sep 24, 2014
 *      Author: joseph07
 */

#ifndef NORMALSAMPLE_H_
#define NORMALSAMPLE_H_

#include "AllelicState.h"

class NormalSample {
public:
	NormalSample();
	virtual ~NormalSample();
	static const vector <AllelicState> create_normal_states();
	static const vector <AllelicState> get_normal_states();

private:
    static const vector <AllelicState> states;

};

#endif /* NORMALSAMPLE_H_ */
