/*
 * Gibbs.cpp
 *
 *  Created on: Sep 20, 2014
 *      Author: malvinajosephidou
 */

#include "Gibbs.h"
#include "utils.h"
bool Gibbs::getIs_normal() const
{
    return is_normal;
}

void Gibbs::setIs_normal(bool is_normal)
{
    this->is_normal = is_normal;
}

MAPstate Gibbs::getMAPstate() {
	MAPstate state;
	vector <int> counts_per_state  = addIntegerVectors(countsBin1, countsBin2);
	state.init(counts_per_state);
	return state;
}
