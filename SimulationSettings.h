/*
 * SimulationSettings.h
 *
 *  Created on: Oct 10, 2014
 *      Author: joseph07
 */

#ifndef SIMULATIONSETTINGS_H_
#define SIMULATIONSETTINGS_H_

#include <string>
using namespace std;

class SimulationSettings {
public:
		double MAF;
		int sims;
		double pError;
		double impurity;
		bool isShared;
		int depth;
		SimulationSettings();
};

#endif /* SIMULATIONSETTINGS_H_ */
