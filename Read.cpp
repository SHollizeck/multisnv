/*
 * Read.cpp
 *
 *  Created on: Feb 19, 2015
 *      Author: joseph07
 */

#include "Read.h"


Read::~Read() {
	// TODO Auto-generated destructor stub
}

Read::Read() {}

Read::Read(int baseIdx, long double err,
		int start_dist, int end_dist, bool is_rev) :
		base(baseIdx),
		error_prob(err),
		dist_start(start_dist),
		dist_end(end_dist),
		isReverse(is_rev)
		{}

