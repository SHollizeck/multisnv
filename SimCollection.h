/*
 * SimCollection.h
 *
 *  Created on: Oct 10, 2014
 *      Author: joseph07
 */

#ifndef SIMCOLLECTION_H_
#define SIMCOLLECTION_H_
#include "DataCollection.h"
#include "SimulationSettings.h"
#include "MultisampleData.h"

class SimCollection : public MultisampleData {
public:
	SimCollection(int N);
	void SimulateData(const SimulationSettings sim_settings);
};

#endif /* SIMCOLLECTION_H_ */
