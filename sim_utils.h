

/*
 *  sim_utils.h
 *  multisnv_proj
 *
 *  Created by Malvina Josephidou on 31/08/2014.
 *  Copyright 2014 University of Cambridge. All rights reserved.
 *
 */
#ifndef ____sim_utils__
#define ____sim_utils__
#include <iostream>
#include <string>
#include <vector>
#include<fstream>
#include<istream>
#include<sstream>
#include <cmath>
#include <stdio.h> //to use fopen etc
#include <vector>
#include <algorithm> // for copy
#include <iterator> // for ostream_iterator
#include <stdlib.h>
#include <map>
#include <utility>
#include <cstring>
#include <iterator>
#include <map>
#include <RInside.h>
#include <Rcpp.h>

#include "Settings.h"
#include "SimulationSettings.h"
#include "GibbsSettings.h"
#include "SummaryResults.h"

using namespace std;

int AddNoise (int allele,  double pError);

void write_vcf_sim_header(const FilterSettings& filter_settings,
		const ProgramSettings& program_settings, const GibbsSettings& gibbs_settings,
		const SimulationSettings& sim_settings);


void run_multisnv_sim(const ProgramSettings& program_settings, const GibbsSettings & gibbs_settings,
		const SimulationSettings& sim_settings, RInside &R);

#endif //sim_utils.h
