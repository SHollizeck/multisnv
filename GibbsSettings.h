/*
 *  GibbsSettings.h
 *  multisnv_proj
 *
 *  Created by Malvina Josephidou on 31/08/2014.
 *  Copyright 2014 University of Cambridge. All rights reserved.
 *
 */
#ifndef ____GibbsSettings__
#define ____GibbsSettings__

class GibbsSettings {
public:	
    double burnin;
    double mutrate;
	// low_depth;  /* depreciated */
    //double SB_threshold;
    int gibbs_runs;
    long double alpha;
    long double alphaN;
    bool checkConv;
    int thin;
    long double weight;
    int maxIter;
    int MedianTumourCoverage;
    int MedianNormalCoverage;

    GibbsSettings():
        burnin(0.1),
        mutrate(0.000003),
//        SB_threshold(0.01),
        gibbs_runs(600),
        alpha(8.0),
		alphaN(50),
        checkConv(false),
        thin(5), /* changed from 0 to 5 in default setting */
        weight(1),
        maxIter(3000),
        MedianTumourCoverage(40),
        MedianNormalCoverage(40) 
    {}
};

#endif // GibbsSettings
