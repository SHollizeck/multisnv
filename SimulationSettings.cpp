/*
 * SimulationSettings.cpp
 *
 *  Created on: Oct 10, 2014
 *      Author: joseph07
 */
#include "SimulationSettings.h"

SimulationSettings::SimulationSettings() :
		MAF(0.2),
		sims(10000),
		pError(0.01),
		impurity(0.0),
		isShared(true),
		depth(30)
{}
