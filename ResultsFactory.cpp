/*
 * ResultsFactory.cpp
 *
 *  Created on: Feb 26, 2015
 *      Author: joseph07
 */

#include "ResultsFactory.h"
#include "SampleResults.h"
#include "Settings.h"

using namespace std;

ResultsFactory getResultsFactory(const GibbsSampler &output, char ref) {
	MAPstate matched_normal_state = output.samplerHistory.at(0)->getMAPstate();

	string reference(1, toupper(ref));
	SampleResults normal_sample(output.samplerHistory.at(0), matched_normal_state, reference);

	vector <SampleResults> tumour_samples;
	for(int i = 1 ; i < output.samplerHistory.size(); i++) {
		tumour_samples.push_back(SampleResults(output.samplerHistory.at(i), matched_normal_state, reference));
	}
	vector <SampleResults> unique_tumour_samples = get_mutated_tumour_states(tumour_samples, normal_sample);
	variant_type_t event_type = get_event_type(unique_tumour_samples, normal_sample);
	ResultsFactory results_factory(normal_sample, tumour_samples, event_type);
	return results_factory;
}

Results* ResultsFactory::create(const map <variant_type_t, bool > &user_is_interested, char ref) {
	if(!user_is_interested.at(event_type)) {
		return new Results(normal_sample, tumour_samples, event_type);
	}
	else {
		if(event_type == LOH ) {
			return new ReportedResultsLOH(normal_sample, tumour_samples, event_type, ref);
		}
		else if(event_type == SOMATIC ) {
			return new ReportedResultsSNV(normal_sample, tumour_samples, event_type, ref);
		}

		else if(event_type == UNEXPECTED ) {
			return new ReportedResultsUnexpectedEvent(normal_sample, tumour_samples, event_type, ref);
		}

		else if(event_type == GERMLINE ) {
			return new ReportedResultsGermline(normal_sample, tumour_samples, event_type, ref);
		}

		else if(event_type == WILDTYPE ) {
			return new ReportedResultsWildtype(normal_sample, tumour_samples, event_type, ref);
		}
	}
	return new Results(normal_sample, tumour_samples, event_type);
}


ResultsFactory::ResultsFactory(SampleResults normal_sample_t, vector <SampleResults> tumour_samples_t,
							   variant_type_t event_type_t)
								: normal_sample(normal_sample_t)
								, tumour_samples(tumour_samples_t)
								, event_type(event_type_t)
								  {}

variant_type_t ResultsFactory::getEvent_type() const {
    return event_type;
}

SampleResults ResultsFactory::getNormal_sample() const {
    return normal_sample;
}

vector<SampleResults> ResultsFactory::getTumour_samples() const {
    return tumour_samples;
}


