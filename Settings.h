//
//  class.h
//  
//
//  Created by Malvina Josephidou on 06/02/2013.
//
//

#ifndef ____class__
#define ____class__

#include <iostream>
#include <string>
#include <vector>
#include<fstream>
#include<istream>
#include<sstream>
#include <cmath>
#include <stdio.h> //to use fopen etc
#include <vector>
#include <algorithm> // for copy
#include <iterator> // for ostream_iterator
#include <stdlib.h>
#include <map>
#include <utility>
#include <cstring>
#include <iterator>
#include <map>


using namespace std;
#define BUFFER_LENGTH_LINE 1024*36

typedef map<char, long double> mapType;

enum variant_type_t { WILDTYPE = 0, GERMLINE = 1, SOMATIC = 2, LOH = 3, UNEXPECTED = 4 };

class ProgramSettings {

public:
    map <variant_type_t, bool> which_events;
    int NumberOfSamples;
    int seed;
    bool print;

    vector <string> bam_files;
    vector <string > regions;
    string filename_output;
    string ref_fasta;
    string bed_file;
    string features_file;
    
    ProgramSettings() {
        which_events[WILDTYPE] = 0;
        which_events[GERMLINE] = 0;
        which_events[SOMATIC] = 1;
        which_events[LOH] = 1;
        which_events[UNEXPECTED] = 1;
        NumberOfSamples = 0;
        seed = 0;
        print = false;
    }
};

class FilterSettings {
public:
    int minVarAlleles;
    int min_depth;
    int max_depth;
    int minVariantReadsForTriallelicSite;
    int flag_low_depth;
    int homopolLengthThreshold;
    bool Rmdups;
    int MappingQualThreshold;
    int ReadQualThreshold;
    double weakEvidenceThreshold;
    double SB_threshold;
    double normalContaminationThreshold;
    double fraction_failing_QC;
    double flag_low_SNV_qual;
    
    FilterSettings()
    	: minVarAlleles (2)
        , min_depth (5)
        , max_depth (200)
    	, minVariantReadsForTriallelicSite(2)
    	, flag_low_depth (6)
    	, homopolLengthThreshold(5)
    	, Rmdups(true)
    	, MappingQualThreshold(30)
    	, ReadQualThreshold(20)
    	, weakEvidenceThreshold(0.10)
    	, SB_threshold(0.01)
    	, normalContaminationThreshold(0.03)
    	, fraction_failing_QC(0.7)
    	, flag_low_SNV_qual(3)
        {}
};

#endif /* defined(____class__) */
