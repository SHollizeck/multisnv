/*
 * NormalSample.cpp
 *
 *  Created on: Sep 24, 2014
 *      Author: joseph07
 */

#include "NormalSample.h"
const vector <AllelicState> NormalSample::states = NormalSample::create_normal_states();

const vector <AllelicState> NormalSample::create_normal_states() {
		vector <AllelicState> normal_states;
		normal_states.push_back(AllelicState(1));
		normal_states.push_back(AllelicState(2));
		normal_states.push_back(AllelicState(3));
		normal_states.push_back(AllelicState(4));
		normal_states.push_back(AllelicState(5));
		normal_states.push_back(AllelicState(6));
		normal_states.push_back(AllelicState(8));
		normal_states.push_back(AllelicState(9));
		normal_states.push_back(AllelicState(10));
		normal_states.push_back(AllelicState(12));
		return normal_states;
}

const vector <AllelicState> NormalSample::get_normal_states() {
	return NormalSample::states;
}

NormalSample::NormalSample() {
	// TODO Auto-generated constructor stub

}

NormalSample::~NormalSample() {
	// TODO Auto-generated destructor stub
}




