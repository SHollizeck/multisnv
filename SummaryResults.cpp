/*
 * SummaryResults.cpp
 *
 *  Created on: Sep 19, 2014
 *      Author: malvinajosephidou
 */

#include "SummaryResults.h"

const map<variant_type_t,string> SummaryResults::event_types = SummaryResults::create_event_map();
bool SummaryResults::instance_flag = false;
SummaryResults* SummaryResults::singleton = 0;

SummaryResults* SummaryResults::getInstance() {
		if(!instance_flag) {
			singleton = new SummaryResults();
			instance_flag = true;
		}
		return singleton;
}

void SummaryResults::destroyInstance() {
		if(instance_flag == true) {
			delete singleton;
			instance_flag = false;
		}
}

void SummaryResults::updateSummary(variant_type_t event_type, bool failed_to_converge) {
    if(failed_to_converge) {
    	n_failed_converge++;
    }
	n_event_type[event_type]++;
	n_runs++;
}

const map <variant_type_t, string> SummaryResults::create_event_map() {
	map <variant_type_t , string> events;
	events[WILDTYPE] = "Wildtype ";
	events[GERMLINE] = "Germline ";
	events[SOMATIC]  = "Somatic ";
	events[LOH]      = "LOH ";
	events[UNEXPECTED]  = "Unknown ";
	return events;
}

SummaryResults::~SummaryResults() {
}

SummaryResults::SummaryResults() :
		n_strand_bias(0),
		n_failed_converge(0),
		n_runs(0),
		n_sites(0) {
		n_event_type[WILDTYPE] = 0;
		n_event_type[GERMLINE] = 0;
		n_event_type[SOMATIC]  = 0;
		n_event_type[LOH]      = 0;
		n_event_type[UNEXPECTED]  = 0;
}

void SummaryResults::print_summary(double runtime) {
		cout << endl;
        cout << "multiSNV has finished processing. " << endl;
        cout << endl;
        cout << "Summary of variant calling statistics: " << endl;
        cout << endl;

		cout << "Number of processed genomic sites: " << n_sites << endl;
        cout << "Somatic site candidates: "          << n_runs << endl;

        cout << "Total number of sites that failed to converge: " << n_failed_converge << endl;

        cout << "Heterozygous Germline sites: " << n_event_type.at(GERMLINE) << endl;
        cout << "Somatic sites: "  << n_event_type.at(SOMATIC) << endl;
        cout << "LOH sites: "      << n_event_type.at(LOH) << endl;
        cout << endl;
        cout << "Total run time: "  << runtime << endl;
        return;
}



