/*
 * ResultsFactory.h
 *
 *  Created on: Feb 26, 2015
 *      Author: joseph07
 */

#ifndef RESULTSFACTORY_H_
#define RESULTSFACTORY_H_
#include "SampleResults.h"
#include "Results.h"
#include <vector>

using namespace std;

class ResultsFactory {
protected:
	SampleResults normal_sample;
	vector <SampleResults> tumour_samples;
	variant_type_t event_type;
public:
	ResultsFactory(SampleResults normal_sample_t, vector <SampleResults> tumour_samples_t,
			variant_type_t event_type_t);
	Results* create(const map <variant_type_t, bool > &user_is_interested, char ref);
    variant_type_t getEvent_type() const;
    SampleResults getNormal_sample() const;
    vector<SampleResults> getTumour_samples() const;
};
ResultsFactory getResultsFactory(const GibbsSampler &output, char ref);
#endif /* RESULTSFACTORY_H_ */
