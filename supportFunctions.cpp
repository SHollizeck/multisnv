//
//  rungibbs.cpp
//  
//
//  Created by Malvina Josephidou on 06/02/2013.
//
//

#include <iostream>
#include <vector>
#include <iomanip>
#include <string>
#include <cstring>
#include<fstream>
#include<istream>
#include<string>
#include<sstream>
#include <algorithm> // for copy
#include <iterator> // for ostream_iterator
#include <stdlib.h>
#include <map>
#include <string.h>
#include <utility>
#include <cstring>
#include <iterator>
#include<sstream>
#include <cmath>
#include <cassert>
#include <stdio.h> //to use fopen etc
#include <algorithm> // for copy
#include <iterator> // for ostream_iterator
#include <stdlib.h>
#include <cstdlib>
#include <ctime>
#include <numeric>
#include <functional>
#include <boost/math/distributions/hypergeometric.hpp>
#include <stdexcept>

#include "rungibbs.h"
#include "Settings.h"

using namespace boost::math;
using namespace std;

/* implements log sum exp trick to avoid underflow */
vector <long double> safe_exponentiation(const vector<long double>& probs) {

    vector <long double > weights(probs.size(),0.0);

    long double max_likelihood = *max_element(probs.begin(), probs.end());
    long double centered_sum = 0;
    for(int i = 0; i < probs.size(); i++) {
        centered_sum = centered_sum + exp( probs.at(i) - max_likelihood);
    }

    for(int i = 0; i < probs.size(); i++) {
        weights.at(i) = exp(probs.at(i) - max_likelihood - log(centered_sum));
    }
    
   assert(fabs(1 - accumulate(weights.begin(),weights.end(),0.0)) <= EPSILON);

    return weights;
}

double fisher_test(const unsigned &a, const unsigned &b, const unsigned &c, const unsigned &d) {
    unsigned N = a + b + c + d;
    unsigned r = a + c;
    unsigned n = c + d;
    unsigned max_for_k = min(r, n);
    unsigned min_for_k = (unsigned)max(0, int(r + n - N));
    hypergeometric_distribution<> hgd(r, n, N);
    double cutoff = pdf(hgd, c);
    double tmp_p = 0.0;
    for(int k = min_for_k;k < max_for_k + 1;k++) {
        double p = pdf(hgd, k);
        if(p <= cutoff) tmp_p += p;
    }
    return tmp_p;
}

string add_delimiter_to_string(string input, string delimiter) {

	string res;
	if(input.size() > 1) {
		res = input[0] + delimiter +
				add_delimiter_to_string(input.substr(1), delimiter);
		return res;
	}

	else {
		return input;
	}
}





