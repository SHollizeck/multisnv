/*
 * AllelicState.cpp
 *
 *  Created on: Sep 20, 2014
 *      Author: malvinajosephidou
 */

#include "AllelicState.h"
#include "MultisampleData.h"

bool AllelicState::operator==(const AllelicState& rhs) const {
	bool isEqual = this->integer == rhs.integer;
	return isEqual;
}

bool AllelicState::operator<(const AllelicState& rhs) const {
	bool isLess = this->integer < rhs.integer;
	return isLess;
}

AllelicState::AllelicState(int x) {
	integer = x;
	is_allele_present = integer_to_binary(x);
	alleles = form_alleles_string(is_allele_present);
}

AllelicState::AllelicState() {
	integer = 0;
	is_allele_present = integer_to_binary(0);
	alleles = form_alleles_string(is_allele_present);
}

string AllelicState::getAlleles() const {
    return alleles;
}

int AllelicState::getInteger() const {
    return integer;
}

const vector<bool> &AllelicState::getIs_allele_present() const {
    return is_allele_present;
}

string AllelicState::form_alleles_string(const vector <bool> &is_allele_present) {
	string alleles = "";
	string bases = "ACGT";
	for(size_t  i = 0 ; i < is_allele_present.size(); i++){
		if(is_allele_present.at(i) == true) {
			alleles = alleles + bases.at(i);
		}
	}
	return alleles;
}

void AllelicState::setAlleles(string alleles) {
    this->alleles = alleles;
}

void AllelicState::setInteger(int integer) {
    this->integer = integer;
}

void AllelicState::setIs_allele_present(const vector<bool> &is_allele_present) {
    this->is_allele_present = is_allele_present;
}

vector <bool> AllelicState::integer_to_binary(int x) {
	vector <bool> is_allele_present(4, false);
	int i = 4;
	while (x > 0) {
		is_allele_present.at(--i) = (x%2 == 0 ? false : true);
		x = x/2;
	}
	return is_allele_present;
}

int AllelicState::total_reads_matching_alleles_in_state(const vector <unsigned int> &total_reads)
{
	int total_reads_inState = 0;
	for (size_t i = 0 ; i < this->is_allele_present.size(); i++){
		total_reads_inState = is_allele_present.at(i) == 1
						? total_reads_inState +  total_reads.at(i)
						: total_reads_inState;
	}
	return total_reads_inState;
}


